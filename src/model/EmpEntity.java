package model;

import java.math.BigInteger;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

public class EmpEntity {
    private int empNo;
    private String cognom;
    private String ofici;
    private Date dataAlta;
    private BigInteger salari;
    private BigInteger comissio;
    private Collection<ClientEntity> clientsByEmpNo;
    private EmpEntity empByCap;
    private Collection<EmpEntity> empsByEmpNo;
    private DeptEntity deptByDeptNo;

    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public Date getDataAlta() {
        return dataAlta;
    }

    public void setDataAlta(Date dataAlta) {
        this.dataAlta = dataAlta;
    }

    public BigInteger getSalari() {
        return salari;
    }

    public void setSalari(BigInteger salari) {
        this.salari = salari;
    }

    public BigInteger getComissio() {
        return comissio;
    }

    public void setComissio(BigInteger comissio) {
        this.comissio = comissio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpEntity empEntity = (EmpEntity) o;
        return empNo == empEntity.empNo && Objects.equals(cognom, empEntity.cognom) && Objects.equals(ofici, empEntity.ofici) && Objects.equals(dataAlta, empEntity.dataAlta) && Objects.equals(salari, empEntity.salari) && Objects.equals(comissio, empEntity.comissio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empNo, cognom, ofici, dataAlta, salari, comissio);
    }

    public Collection<ClientEntity> getClientsByEmpNo() {
        return clientsByEmpNo;
    }

    public void setClientsByEmpNo(Collection<ClientEntity> clientsByEmpNo) {
        this.clientsByEmpNo = clientsByEmpNo;
    }

    public EmpEntity getEmpByCap() {
        return empByCap;
    }

    public void setEmpByCap(EmpEntity empByCap) {
        this.empByCap = empByCap;
    }

    public Collection<EmpEntity> getEmpsByEmpNo() {
        return empsByEmpNo;
    }

    public void setEmpsByEmpNo(Collection<EmpEntity> empsByEmpNo) {
        this.empsByEmpNo = empsByEmpNo;
    }

    public DeptEntity getDeptByDeptNo() {
        return deptByDeptNo;
    }

    public void setDeptByDeptNo(DeptEntity deptByDeptNo) {
        this.deptByDeptNo = deptByDeptNo;
    }

    @Override
    public String toString() {
        return "EmpEntity{" +
                "empNo=" + empNo +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", dataAlta=" + dataAlta +
                ", salari=" + salari +
                ", comissio=" + comissio +
                '}';
    }
}
