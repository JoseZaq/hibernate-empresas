package model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

public class ClientEntity {
    private int clientCod;
    private String nom;
    private String adreca;
    private String ciutat;
    private String estat;
    private String codiPostal;
    private Integer area;
    private String telefon;
    private BigDecimal limitCredit;
    private String observacions;
    private EmpEntity empByReprCod;
    private Collection<ComandaEntity> comandasByClientCod;

    public int getClientCod() {
        return clientCod;
    }

    public void setClientCod(int clientCod) {
        this.clientCod = clientCod;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public BigDecimal getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(BigDecimal limitCredit) {
        this.limitCredit = limitCredit;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return clientCod == that.clientCod && Objects.equals(nom, that.nom) && Objects.equals(adreca, that.adreca) && Objects.equals(ciutat, that.ciutat) && Objects.equals(estat, that.estat) && Objects.equals(codiPostal, that.codiPostal) && Objects.equals(area, that.area) && Objects.equals(telefon, that.telefon) && Objects.equals(limitCredit, that.limitCredit) && Objects.equals(observacions, that.observacions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientCod, nom, adreca, ciutat, estat, codiPostal, area, telefon, limitCredit, observacions);
    }

    public EmpEntity getEmpByReprCod() {
        return empByReprCod;
    }

    public void setEmpByReprCod(EmpEntity empByReprCod) {
        this.empByReprCod = empByReprCod;
    }

    public Collection<ComandaEntity> getComandasByClientCod() {
        return comandasByClientCod;
    }

    public void setComandasByClientCod(Collection<ComandaEntity> comandasByClientCod) {
        this.comandasByClientCod = comandasByClientCod;
    }

    @Override
    public String toString() {
        return "ClientEntity{" +
                "clientCod=" + clientCod +
                ", nom='" + nom + '\'' +
                ", adreca='" + adreca + '\'' +
                ", ciutat='" + ciutat + '\'' +
                ", estat='" + estat + '\'' +
                ", codiPostal='" + codiPostal + '\'' +
                ", area=" + area +
                ", telefon='" + telefon + '\'' +
                ", limitCredit=" + limitCredit +
                ", observacions='" + observacions + '\'' +
                ", empByReprCod=" + empByReprCod +
                '}';
    }
}
