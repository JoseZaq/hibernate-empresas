package model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

public class ComandaEntity {
    private int comNum;
    private Date comData;
    private String comTipus;
    private Date dataTramesa;
    private BigDecimal total;
    private ClientEntity clientByClientCod;
    private Collection<DetallEntity> detallsByComNum;

    public int getComNum() {
        return comNum;
    }

    public void setComNum(int comNum) {
        this.comNum = comNum;
    }

    public Date getComData() {
        return comData;
    }

    public void setComData(Date comData) {
        this.comData = comData;
    }

    public String getComTipus() {
        return comTipus;
    }

    public void setComTipus(String comTipus) {
        this.comTipus = comTipus;
    }

    public Date getDataTramesa() {
        return dataTramesa;
    }

    public void setDataTramesa(Date dataTramesa) {
        this.dataTramesa = dataTramesa;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComandaEntity that = (ComandaEntity) o;
        return comNum == that.comNum && Objects.equals(comData, that.comData) && Objects.equals(comTipus, that.comTipus) && Objects.equals(dataTramesa, that.dataTramesa) && Objects.equals(total, that.total);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comNum, comData, comTipus, dataTramesa, total);
    }

    public ClientEntity getClientByClientCod() {
        return clientByClientCod;
    }

    public void setClientByClientCod(ClientEntity clientByClientCod) {
        this.clientByClientCod = clientByClientCod;
    }

    public Collection<DetallEntity> getDetallsByComNum() {
        return detallsByComNum;
    }

    public void setDetallsByComNum(Collection<DetallEntity> detallsByComNum) {
        this.detallsByComNum = detallsByComNum;
    }

    @Override
    public String toString() {
        return "ComandaEntity{" +
                "comNum=" + comNum +
                ", comData=" + comData +
                ", comTipus='" + comTipus + '\'' +
                ", dataTramesa=" + dataTramesa +
                ", total=" + total +
                '}';
    }
}
