package model;

import java.util.Collection;
import java.util.Objects;

public class ProducteEntity {
    private int prodNum;
    private String descripcio;
    private Collection<DetallEntity> detallsByProdNum;

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProducteEntity that = (ProducteEntity) o;
        return prodNum == that.prodNum && Objects.equals(descripcio, that.descripcio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prodNum, descripcio);
    }

    public Collection<DetallEntity> getDetallsByProdNum() {
        return detallsByProdNum;
    }

    public void setDetallsByProdNum(Collection<DetallEntity> detallsByProdNum) {
        this.detallsByProdNum = detallsByProdNum;
    }

    @Override
    public String toString() {
        return "ProducteEntity{" +
                "prodNum=" + prodNum +
                ", descripcio='" + descripcio + '\'' +
                '}';
    }
}
