package model;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Objects;

public class DeptEntity {
    private BigInteger deptNo;
    private String dnom;
    private String loc;
    private Collection<EmpEntity> empsByDeptNo;

    public BigInteger getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(BigInteger deptNo) {
        this.deptNo = deptNo;
    }

    public String getDnom() {
        return dnom;
    }

    public void setDnom(String dnom) {
        this.dnom = dnom;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeptEntity that = (DeptEntity) o;
        return Objects.equals(deptNo, that.deptNo) && Objects.equals(dnom, that.dnom) && Objects.equals(loc, that.loc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptNo, dnom, loc);
    }

    public Collection<EmpEntity> getEmpsByDeptNo() {
        return empsByDeptNo;
    }

    public void setEmpsByDeptNo(Collection<EmpEntity> empsByDeptNo) {
        this.empsByDeptNo = empsByDeptNo;
    }

    @Override
    public String toString() {
        return "DeptEntity{" +
                "deptNo=" + deptNo +
                ", dnom='" + dnom + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }
}
