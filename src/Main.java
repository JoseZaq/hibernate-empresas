import CRUD.ClientCRUD;
import CRUD.ComandaCRUD;
import CRUD.EmpCRUD;
import model.ClientEntity;
import model.ComandaEntity;
import model.DeptEntity;
import model.EmpEntity;
import org.hibernate.Session;
import utils.Menu;

import java.math.BigInteger;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    static ComandaCRUD comandaCrud;
    static ClientCRUD clientCrud;
    static EmpCRUD empCrud;
    public static void main(final String[] args) throws Exception {
        final Session session = ConnexioEmpresa.getSession();
        final String[] options = {"Ex1-Mostra tota la informació de totes les comandes.",
                "Ex2- Mostra el nom del client (client_cod) i la data de comanda (com_data) " +
                        "de les comandes que el camp com_tipus sigui null",
                "Ex3- Mostra el nom i el telèfon del clients de la ciutat de BURLINGAME",
                "Ex4- Mostra tota la informació de l’empleat amb cognom CEREZO",
                "Ex5- Mostra el codi (com_num) de les comandes que la data de tramesa sigui entre el 1 de gener del 1986" +
                        " i el 30 de setembre del 1986. Utilitza el CriteriaBuilder.between.",
                "Ex6- Mostra el cognom i l’ofici dels empleats que cobrin més de 300.000",
                "Ex7- Mostra quants empleats té cada departament. Utilitza el CriteriaQuery.groupBy"};
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        comandaCrud = new ComandaCRUD(session);
        clientCrud = new ClientCRUD(session);
        empCrud = new EmpCRUD(session);
        try {
            Menu menu = new Menu(options,scanner){
                @Override
                public void goToMethod(int option) {
                    switch (option){
                        case 1:
                            ex1ShowComandes();
                            break;
                        case 2:
                            ex2ShowUntypedComandes();
                            break;
                        case 3:
                            ex3ShowClientsFromBurlingame();
                            break;
                        case 4:
                            ex4ShowDataOfClient();
                            break;
                        case 5:
                            ex5ShowComandesBetweenDates();
                            break;
                        case 6:
                            ex6ShowAllEmpBySalary();
                            break;
                        case 7:
                            ex7ShowEmpQuantityEachDept();
                    }
                }
            };
            menu.start();
        } finally {
            session.close();
        }
    }

    /**
     * Ex7- Mostra quants empleats té cada departament. Utilitza el CriteriaQuery.groupBy
     */
    private static void ex7ShowEmpQuantityEachDept() {
        List<Object[]> emps = empCrud.getEmpQuantityToEachDept();
        for (Object[] emp : emps) {
            DeptEntity dep = (DeptEntity) emp[0];
            Long empQ = (Long) emp[1];
            System.out.println("Departamento: " + dep.getDnom()+
                    ", No Empleats: "+empQ);
        }
    }

    /**
     * Excercici 6. Mostra el cognom i l’ofici dels empleats que cobrin més de 300.000.
     */
    private static void ex6ShowAllEmpBySalary() {
        List<ClientEntity> emps = empCrud.getEmpBySalary(300000);
        for( ClientEntity emp: emps){
            System.out.println(emp);
        }
    }


    /**
     * Exercici 5. Mostra el codi (com_num) de les comandes que la data de tramesa sigui entre el 1 de gener del 1986 i el
     * 30 de setembre del 1986. Utilitza el CriteriaBuilder.between.
     */
    private static void ex5ShowComandesBetweenDates() {
        List<ComandaEntity> allComands = comandaCrud.getBetweenDates();
        for (ComandaEntity all : allComands){
            System.out.println("com_num: "+all.getComNum());
        }
    }

    /**
     *Exercici 4. Mostra tota la informació de l’empleat amb cognom CEREZO
     */
    private static void ex4ShowDataOfClient() {
        EmpEntity emp = empCrud.getEmpByLastname("CEREZO");
        System.out.println(emp);
    }

    /**
     * Exercici 3. Mostra el nom i el telèfon del clients de la ciutat de BURLINGAME
     */
    private static void ex3ShowClientsFromBurlingame() {
        List<ClientEntity> clients = clientCrud.getAllByCity("BURLINGAME");
        for( ClientEntity client: clients){
            System.out.println("Nom: "+client.getNom()+", Telefon: "+client.getTelefon());
        }
    }

    /**
     * Exercici 2. Mostra el nom del client (client_cod) i la data de comanda (com_data) de les comandes que el camp
     * com_tipus sigui null
     */
    private static void ex2ShowUntypedComandes() {
        List<ComandaEntity> allComands = comandaCrud.getALlUntyped();
        for (ComandaEntity all : allComands){
            System.out.println("Client: "+all.getClientByClientCod().getClientCod()+
                    ", Data de comanda: "+all.getComData().toString());
        }
    }

    /**
     * Exercici 1. Mostra tota la informació de totes les comandes.
     */
    private static void ex1ShowComandes() {
        List<ComandaEntity> allComands = comandaCrud.getAll();
        for (ComandaEntity all : allComands){
            System.out.println(all);
        }
    }
}