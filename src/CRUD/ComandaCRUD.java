package CRUD;

import model.ComandaEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

public class ComandaCRUD {
    // vars
    private Session session;
    // constructor
    public ComandaCRUD(Session session) {
        this.session = session;
    }
    // mehods
    public List getAll(){
        List comandas;
        Criteria crit = session.createCriteria(ComandaEntity.class);
        comandas = crit.list();
        return comandas;
    }
    public List getALlUntyped(){
        List comandas;
        Criteria crit = session.createCriteria(ComandaEntity.class);
        comandas = crit.add(Restrictions.isNull("comTipus")).list();
        return comandas;
    }
    public List getBetweenDates(){
        List comandas;
//        CriteriaBuilder critBuilder = session.getCriteriaBuilder();
//        CriteriaQuery<ComandaEntity> crit = critBuilder.createQuery(ComandaEntity.class);
//        Root<ComandaEntity> root = crit.from(ComandaEntity.class);
//        critBuilder.between(root.get("dataTramesa"),Date.UTC(1986,Calendar.JANUARY,1,0,0,0),
//                Date.UTC(1986, Calendar.SEPTEMBER,30,0,0,0));
//        TypedQuery<ComandaEntity> query = session.createQuery(crit);
//
//        comandas = query.getResultList();

        Criteria crt = session.createCriteria(ComandaEntity.class);
        comandas = crt.add(Restrictions.between("dataTramesa",
                Date.valueOf("1986-01-01"),
                Date.valueOf("1986-07-30"))).list();
        return comandas;


    }
}
