package CRUD;

import model.ClientEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class ClientCRUD {
    // vars
    private Session session;
    // constructor
    public ClientCRUD(Session session) {
        this.session = session;
    }
    // methods
    public List getAllByCity(String city){
        List clients;
        Criteria crit = session.createCriteria(ClientEntity.class);
        clients = crit.add(Restrictions.like("ciutat", city+"%")).list();
        return clients;
    }
}
