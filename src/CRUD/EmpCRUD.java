package CRUD;

import model.EmpEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.List;

public class EmpCRUD {
    // vars
    private Session session;
    // constructor
    public EmpCRUD(Session session) {
        this.session = session;
    }
    // methods
    public EmpEntity getEmpByLastname(String lastname){
        EmpEntity emp;
        Criteria crit = session.createCriteria(EmpEntity.class);
        emp = (EmpEntity) crit.add(Restrictions.like("cognom",lastname)).uniqueResult();
        return emp;
    }
    public List getEmpBySalary(int salary){
        List emp;
        Criteria crit = session.createCriteria(EmpEntity.class);
        emp = crit.add(Restrictions.like("salari", BigInteger.valueOf(100000L))).list();
        return emp;
    }

    public List getEmpQuantityToEachDept(){
        Criteria crit = session.createCriteria(EmpEntity.class);

        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.groupProperty("deptByDeptNo"));
        projectionList.add(Projections.rowCount());

        return crit.setProjection(projectionList).list();
    }
}
